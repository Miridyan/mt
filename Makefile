# Just tell make that clean, install, and uninstall doesn't generate files
.PHONY: clean install uninstall

# # Build the release of the application
# build : 
#    cargo build --release

build-devel :
    cargo build

# Install the application
# Should fix this at some point to install to a specified $PREFIX, but I don't know how to do that.
# Until then, I'm going to default to `/usr`
install :
    mkdir -p /usr/local/bin
    mkdir -p /usr/local/share/Mt/profiles
    mkdir -p /usr/local/share/applications
    mkdir -p /usr/local/share/icons/hicolor/scalable/apps

    install -m 755 target/debug/mt /usr/local/bin/Mt
    install -m 644 data/profile/Default.ron /usr/local/share/Mt/profiles/Default.ron
    install -m 644 data/com.gitlab.miridyan.Mt.desktop /usr/local/share/applications/com.gitlab.miridyan.Mt.desktop
    install -m 644 data/icons/com.gitlab.miridyan.Mt.svg /usr/local/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.svg
    install -m 644 data/icons/com.gitlab.miridyan.Mt.Devel.svg /usr/local/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.Devel.svg

# Uninstall the application
uninstall :
    rm /usr/local/bin/Mt
    rm /usr/local/share/Mt/profiles/Default.ron
    rm /usr/local/share/applications/com.gitlab.miridyan.Mt.desktop
    rm /usr/local/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.svg
    rm /usr/local/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.Devel.svg

# Clean directory
clean :
    cargo clean
