use crate::{widget::menu::MtMenu, window::WindowContent};
use glib::clone;
use gtk::{prelude::*, Box, Builder, Label, MenuButton};
use libhandy::{HeaderBar, TabBar, TabView, WindowHandle};

#[derive(Debug)]
pub struct StandardWindowContent {
    root: Box,

    titlebar: HeaderBar,
    titlebar_box: Box,
    windowhandle: WindowHandle,

    tab_bar: TabBar,
    tab_view: TabView,

    menu_button: MenuButton,
    window_title: Label,

    builder: Builder,
}

impl StandardWindowContent {
    pub fn new() -> Self {
        let builder =
            Builder::from_resource("/com/gitlab/miridyan/Mt/ui/standard-window-content.ui");

        let root = builder
            .object::<Box>("root-widget")
            .expect("Failed to load root-widget");
        let titlebar = builder
            .object::<HeaderBar>("headerbar")
            .expect("Failed to load headerbar");
        let windowhandle = builder
            .object::<WindowHandle>("windowhandle")
            .expect("Failed to load windowhandle");

        let titlebar_box = builder
            .object::<Box>("titlebar-box")
            .expect("Failed to load titlebar-box");
        let tab_bar = builder
            .object::<TabBar>("tab-bar")
            .expect("Failed to load tab-bar");
        let tab_view = builder
            .object::<TabView>("tab-view")
            .expect("Failed to load tab-view");

        let menu_button = builder
            .object::<MenuButton>("tab-button")
            .expect("Failed to load tab-button");
        let window_title = builder
            .object::<Label>("window-title")
            .expect("Failed to load window-title");

        tab_bar.connect_tabs_revealed_notify(
            clone!(@weak titlebar_box => move |tab_bar| {
                let revealed_state = tab_bar.is_tabs_revealed();
                let title_style_context = titlebar_box.style_context();

                if revealed_state {
                    title_style_context.add_class("titlebar");
                } else {
                    title_style_context.remove_class("titlebar");
                }
            })
        );

        Self {
            root,
            titlebar,
            titlebar_box,
            windowhandle,
            tab_bar,
            tab_view,
            menu_button,
            window_title,
            builder,
        }
    }
}

impl WindowContent for StandardWindowContent {
    fn tabview(&self) -> &TabView {
        &self.tab_view
    }

    fn tabbar(&self) -> &TabBar {
        &self.tab_bar
    }

    fn set_menu(&self, menu: &MtMenu) {
        self.menu_button.set_popup(Some(&menu.into_menu()));
    }

    fn root(&self) -> &Box {
        &self.root
    }

    fn set_title(&self, title: &str) {
        self.window_title.set_text(title);
    }

    fn set_style(&self, _old: &str, _new: &str) {
        ()
    }
}
