use crate::{widget::menu::MtMenu, window::WindowContent};
use glib::clone;
use gtk::{prelude::*, Box, Builder, Label};
use libhandy::{HeaderBar, TabBar, TabView, WindowHandle};

#[derive(Debug)]
pub struct CompactWindowContent {
    root: Box,

    titlebar: HeaderBar,
    titlebar_box: Box,
    windowhandle: WindowHandle,

    tab_bar: TabBar,
    tab_view: TabView,

    window_title: Label,

    builder: Builder,
}

impl CompactWindowContent {
    pub fn new() -> Self {
        let builder =
            Builder::from_resource("/com/gitlab/miridyan/Mt/ui/compact-window-content.ui");

        let root = builder
            .object::<Box>("root-widget")
            .expect("Failed to load root-widget");
        let titlebar = builder
            .object::<HeaderBar>("headerbar")
            .expect("Failed to load headerbar");
        let windowhandle = builder
            .object::<WindowHandle>("windowhandle")
            .expect("Failed to load windowhandle");

        let titlebar_box = builder
            .object::<Box>("titlebar-box")
            .expect("Failed to load titlebar-box");
        let tab_bar = builder
            .object::<TabBar>("tab-bar")
            .expect("Failed to load tab-bar");
        let tab_view = builder
            .object::<TabView>("tab-view")
            .expect("Failed to load tab-view");

        let window_title = builder
            .object::<Label>("window-title")
            .expect("Failed to load window-title");

        tab_bar.connect_tabs_revealed_notify(
            clone!(@weak titlebar_box => move |tab_bar| {
                let revealed_state = tab_bar.is_tabs_revealed();
                let title_style_context = titlebar_box.style_context();

                if revealed_state {
                    title_style_context.add_class("titlebar");
                } else {
                    title_style_context.remove_class("titlebar");
                }
            })
        );

        Self {
            root,
            titlebar,
            titlebar_box,
            windowhandle,
            tab_bar,
            tab_view,
            window_title,
            builder,
        }
    }
}

impl WindowContent for CompactWindowContent {
    fn tabview(&self) -> &TabView {
        &self.tab_view
    }

    fn tabbar(&self) -> &TabBar {
        &self.tab_bar
    }

    fn set_menu(&self, menu: &MtMenu) {
        self.tab_view.connect_popup_menu(
            clone!(@strong menu => move |tab_view| {
                let menu = menu.into_menu();
                menu.set_attach_widget(Some(tab_view));
                menu.popup_at_pointer(None);

                true
            })
        );

        self.tab_view.connect_event(|t, event| {
            if event.button().is_some() && event.button().unwrap() == 3 {
                t.emit_popup_menu();

                Inhibit(true)
            } else {
                Inhibit(false)
            }
        });
    }

    fn root(&self) -> &Box {
        &self.root
    }

    fn set_title(&self, title: &str) {
        self.window_title.set_text(title);
    }

    fn set_style(&self, _old: &str, _new: &str) {
        ()
    }
}

