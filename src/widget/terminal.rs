//! Out of everything I've implemented in this app, I hate this module the most.

use crate::{
    app::TerminalParams,
    config::{Profile, ScrollbarVisibility},
    message::WindowCommand,
    vte::VteBuilder,
};
use gio::Cancellable;
use glib::{subclass, subclass::prelude::*, translate::ToGlibPtr, Object, Sender, SpawnFlags};
use gtk::{
    prelude::*, subclass::prelude::*, Adjustment, Box, Orientation, PolicyType, ScrolledWindow,
    ScrolledWindowBuilder, ShadowType,
};
use once_cell::unsync::OnceCell;
use std::{
    cell::Cell,
    ops::Deref,
    path::{Path, PathBuf},
    rc::Rc,
};
use vte::{traits::TerminalExt, PtyFlags, Terminal};

#[derive(Clone, Debug)]
pub struct MtTerminalPriv {
    widgets: OnceCell<Widgets>,
    profile: OnceCell<Profile>,
    sender: OnceCell<Sender<WindowCommand>>,
    // Why is this a `Cell<T>` and not a `OnceCell<T>`? I don't know, I want to do something
    // with it later, but I don't know what. It was kind of part of an experiment to detect
    // if a process ends and how it ends. I still need to do this though.
    pid: Rc<Cell<glib::Pid>>,
}

#[derive(Clone, Debug)]
/// The terminal widget contains only two major widgets, a scrolled window (which in turn contains
/// a scrollbar), and the terminal itself.
struct Widgets {
    terminal: Terminal,
    scrolled_window: ScrolledWindow,
}

/// Standard subclassing of GObjects in rust, nothing really special here.
#[glib::object_subclass]
impl ObjectSubclass for MtTerminalPriv {
    const NAME: &'static str = "MtTerminal";

    type Type = MtTerminal;
    type ParentType = Box;
    type Class = subclass::basic::ClassStruct<Self>;
    type Instance = subclass::basic::InstanceStruct<Self>;

    fn new() -> Self {
        Self {
            widgets: OnceCell::new(),
            profile: OnceCell::new(),
            sender: OnceCell::new(),
            pid: Rc::new(Cell::new(glib::Pid(-1i32))),
        }
    }
}

impl ObjectImpl for MtTerminalPriv {}

/// Just a bunch of setters. If any of these fail the app crashes. This is by design.
impl MtTerminalPriv {
    fn set_widgets(&self, widgets: Widgets) {
        self.widgets.set(widgets).expect("Failed to set widgets");
    }

    fn set_profile(&self, profile: Profile) {
        self.profile.set(profile).expect("Failed to set profile");
    }

    fn set_sender(&self, sender: Sender<WindowCommand>) {
        self.sender.set(sender).expect("Failed to set sender");
    }

    fn profile(&self) -> Option<&Profile> {
        self.profile.get()
    }

    fn terminal(&self) -> Option<&Terminal> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.terminal);
        }

        None
    }
}

impl WidgetImpl for MtTerminalPriv {}
impl ContainerImpl for MtTerminalPriv {}
impl BoxImpl for MtTerminalPriv {}

glib::wrapper! {
    /// This struct corresponds to a single VTE Terminal and its corresponding profile. Because multiple
    /// tabs can be open with different profiles, it makes sense to store it with the profile. This will
    /// end up redundant if all the terminals end up with same profile, so eventually this should probably
    /// be refactored to store a UUID for the profile or something and then reference a profile HashMap
    /// in the main Model of the application.
    pub struct MtTerminal(
        ObjectSubclass<MtTerminalPriv>
    ) @extends gtk::Widget, gtk::Container, gtk::Box;

    // match fn {
    //     get_type => || MtTerminalPriv::get_type().to_glib(),
    // }
}

/// Okay, this one is really gross. Lots of just straight up ugly code that I haven't found a
/// to do in a clean way that doesn't make me want to throw up.
impl MtTerminal {
    pub fn new(
        // _w: &MtApplicationWindow,
        t: TerminalParams,
        cwd: Option<PathBuf>,
        cmd: Option<Vec<PathBuf>>,
        sender: Sender<WindowCommand>,
    ) -> Self {
        let box_: MtTerminal = Object::new(&[
            ("hexpand", &true),
            ("vexpand", &true),
            ("orientation", &Orientation::Horizontal),
            ("spacing", &0),
        ])
        .expect("Failed to create MtTerminal");
        // .downcast::<MtTerminal>()
        // .expect("Created MtTerminal is of wrong type");

        let priv_ = MtTerminalPriv::from_instance(&box_);

        // The next four lines are absolute dogshit and I hate them.
        let TerminalParams(p, c, s) = t;
        let (font, allow_bold) = p.font();
        let (bg, fg, _, colorscheme) = c.into_gdk();
        let cmd = if let Some(cmd) = cmd { cmd } else { p.cmd() };

        log::debug!("CMD {:?}", cmd);

        // Runs the risk of losing non-utf8 characters.
        let cwd = cwd
            .and_then(|path| Some(path.to_string_lossy().deref().to_owned()))
            .unwrap_or_else(|| p.cwd.to_owned());

        let terminal = VteBuilder::new()
            .font(font)
            .hexpand(true)
            .vexpand(true)
            .size(p.dimensions)
            .audible_bell(p.audible_bell)
            .allow_bold(allow_bold)
            .colorscheme(&bg, &fg, &colorscheme)
            .scrollback_lines(p.scrollback)
            .cursor_shape(p.cursor_style.into())
            .cursor_blink_mode(p.cursor_blink_mode.into())
            .backspace_binding(p.erase_binding.into())
            .build();

        // Need to add error checking system here
        // Also need to use the asynchronous version of this, sync is deprecated.
        let pid = terminal
            .spawn_sync::<Cancellable>(
                PtyFlags::DEFAULT,
                Some(&cwd),
                &cmd.iter().map(PathBuf::as_path).collect::<Vec<&Path>>(),
                &[],
                SpawnFlags::DEFAULT | SpawnFlags::SEARCH_PATH_FROM_ENVP,
                Some(&mut || {}),
                None,
            )
            .unwrap();

        // Look, I know this is bad, and that I probably shouldn't do this, but right now it makes
        // scrollbars work, so it's what I'm sticking with.
        let adjustment: Adjustment = unsafe {
            let term_ref: &Terminal = terminal.as_ref();
            let scrollable_ptr: *mut vte_sys::VteTerminal = term_ref.to_glib_none().0;
            let adjustment = gtk_sys::gtk_scrollable_get_vadjustment(
                scrollable_ptr as *mut gtk_sys::GtkScrollable,
            );

            glib::translate::from_glib_none(adjustment)
        };

        // I should just force either overlay scrolling or no scrolling at all.
        // Would simplify all this
        let scroll_policy = if s != ScrollbarVisibility::Hidden {
            PolicyType::Automatic
        } else {
            PolicyType::Never
        };

        let overlay_scrolling = s == ScrollbarVisibility::Overlay;
        // let menu = {
        //     let menu_builder = Builder::from_resource("/com/gitlab/miridyan/Mt/ui/context-menu.ui");
        //     let menu_model = menu_builder.get_object::<MenuModel>("context-menu").unwrap();
        //     let menu = Menu::from_model(&menu_model);
        //     menu.set_property_attach_widget(Some(w));
        //     menu
        // };

        terminal.set_visible(true);
        // I don't want to delete this yet in case I need it (I probably don't)

        // terminal.connect_popup_menu(move |_| {
        //     menu.popup_at_pointer(None);

        //     true
        // });
        // terminal.connect_event(|t, event| {
        //     if event.get_button().is_some() && event.get_button().unwrap() == 3 {
        //         t.emit_popup_menu();

        //         Inhibit(true)
        //     } else {
        //         Inhibit(false)
        //     }
        // });

        // Create scrolled window container and give it the gtk::Adjustment from the
        // vte::Terminal
        let scrolled_window = ScrolledWindowBuilder::new()
            .hscrollbar_policy(PolicyType::Never)
            .vscrollbar_policy(scroll_policy)
            .propagate_natural_height(true)
            .propagate_natural_width(true)
            .shadow_type(ShadowType::None)
            .vadjustment(&adjustment)
            .overlay_scrolling(overlay_scrolling)
            .child(&terminal)
            .vexpand(true)
            .hexpand(true)
            .build();

        scrolled_window.set_visible(true);
        box_.add(&scrolled_window);
        let widgets = Widgets {
            terminal,
            scrolled_window,
        };
        priv_.set_sender(sender);
        priv_.set_widgets(widgets);
        priv_.set_profile(p.to_owned());

        (*priv_.pid).set(pid);

        box_
    }

    pub fn profile(&self) -> &Profile {
        let priv_ = MtTerminalPriv::from_instance(self);
        priv_
            .profile()
            .expect("Failed to get profile, should never occur")
    }

    /// When the window first opens, the vte::Terminal doesn't have input focus
    /// immediately. Need to implement this method and execute it whenever a tab
    /// is changed to ensure that the terminal has keyboard focus. If not, the user
    /// has to click the terminal to begin typing, and that's awful.
    pub fn focus(&self) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            terminal.grab_focus();
        }
    }

    /// Doesn't actually get the title of the window, gets the title sent back
    /// by the process that you want to be the window title.
    pub fn get_window_title(&self) -> Option<String> {
        let priv_ = MtTerminalPriv::from_instance(self);

        if let Some(terminal) = priv_.terminal() {
            if let Some(title) = terminal.window_title() {
                return Some(title.as_str().to_owned());
            }
        }

        None
    }

    /// Copy selection in current terminal to the clipboard
    pub fn copy(&self) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            terminal.copy_clipboard();
        }
    }

    /// Paste clipboard into terminal
    pub fn paste(&self) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            terminal.paste_clipboard();
        }
    }

    /// This function is gross for two reasons:
    ///     1. It uses unsafe code (though I guess all of gtk is unsafe under the hood)
    ///     2. If you set geometry hints right after initializing a shell process on a
    ///        vte widget, it inserts an erroneous return (⏎) on the first line and messes
    ///        up resizing. Need to set geometry hints initially and then initilize the shell.
    pub fn set_geometry_hints(&self, w: &gtk::Window) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            unsafe {
                let terminal_ptr: *mut vte_sys::VteTerminal = terminal.to_glib_none().0;
                let window_ptr: *mut gtk_sys::GtkWindow = w.to_glib_none().0;

                vte_sys::vte_terminal_set_geometry_hints_for_window(terminal_ptr, window_ptr);
            }
        }
    }

    /// Get the `Sender<WindowCommand>` for this terminal so we can send messages
    /// to the root window based on changes in the current widget.
    // fn sender(&self) -> &Sender<WindowCommand> {
    //     let priv_ = MtTerminalPriv::from_instance(self);
    //     priv_.sender.get().unwrap()
    // }

    /// Given a header widget and a notebook, make the header widget the notebook
    /// tab title and make the close button close the tab it is attached to.
    /// Should be able to get rid of this once I start using HdyTabView from libhandy 1.2
    /// This is my second least favorite function in this project (my first being
    /// `MtTerminal::new()`)
    // pub fn assign_header(&self, header: &MtTabHeader, notebook: &gtk::Notebook) {
    //     let priv_ = MtTerminalPriv::from_instance(self);
    //     let terminal = priv_
    //         .terminal()
    //         .expect("Failed to get terminal, should never occur");

    //     header.connect_property_tab_close(
    //         clone!(@weak notebook, @weak self as terminal => move|_| {
    //             let _ = notebook
    //                 .page_num(&terminal)
    //                 .and_then(|page| {
    //                     terminal.sender().send(WindowCommand::CloseTab(page)).ok()
    //                 });
    //         }),
    //     );

    //     terminal.connect_child_exited(
    //         clone!(@weak notebook, @weak self as terminal => move|_, _| {
    //             let _ = notebook
    //                 .page_num(&terminal)
    //                 .and_then(|page| {
    //                     terminal.sender().send(WindowCommand::CloseTab(page)).ok()
    //                 });
    //         }),
    //     );

    //     terminal.connect_window_title_changed(
    //         clone!(@weak header, @weak notebook, @weak self as terminal => move |vte| {
    //             let page_num = notebook.page_num(&terminal);
    //             let title = vte.window_title()
    //                 .and_then(|s| Some(s.as_str().to_string()))
    //                 .unwrap_or("".to_string());

    //             if let Some(page) = page_num {
    //                 if let Err(e) = terminal.sender().send(WindowCommand::UpdateTabTitle { page, title: title.clone() }) {
    //                     log::error!("Failed to update tab title {:?}", e);
    //                 }

    //                 if page_num == notebook.current_page() {
    //                     if let Err(e) = terminal.sender().send(WindowCommand::UpdateWindowTitle(title)) {
    //                         log::error!("Failed to update window title {:?}", e);
    //                     }
    //                 }
    //             }
    //         }),
    //     );
    // }

    pub fn connect_window_title_changed<F>(&self, f: F) -> glib::SignalHandlerId
    where
        F: Fn(&Terminal) + 'static,
    {
        let priv_ = MtTerminalPriv::from_instance(self);
        let terminal = priv_
            .terminal()
            .expect("Failed to get terminal, should never occur");

        terminal.connect_window_title_changed(f)
    }
}
