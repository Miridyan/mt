mod defaults;
mod store;

pub use self::{defaults::Defaults, store::Store};

use self::defaults::PROFILE_DEFAULTS;
use super::enums::*;
use crate::util::{self};

use pango::FontDescription;
use serde::{de, Deserialize, Deserializer, Serialize};
use std::{env, ops::Deref, path::PathBuf, result};

/// Structure describing the basic terminal profile (what shell to use, what colorscheme to use,
/// what font to use, what size to make the terminal when it launches for the first time, open new
/// terminals in a new window or new tab, etc, etc, etc).
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Profile {
    #[serde(default = "Profile::default_scrollback")]
    pub scrollback: i64,

    #[serde(default = "Profile::default_dimensions")]
    pub dimensions: (u32, u32),

    #[serde(default = "Profile::default_cell_spacing")]
    pub cell_spacing: (f32, f32),

    #[serde(default = "Profile::default_cursor")]
    pub cursor_style: CursorShape,

    #[serde(default = "Profile::default_blink")]
    pub cursor_blink_mode: CursorBlinkMode,

    #[serde(default = "Profile::default_erase")]
    pub erase_binding: EraseBinding,

    #[serde(default = "Profile::default_bell")]
    pub audible_bell: bool,

    #[serde(default = "Profile::default_colorscheme")]
    pub colorscheme: String,

    #[serde(default = "Profile::default_font")]
    pub font: Font,

    #[serde(deserialize_with = "Profile::deserialize_cmd")]
    pub cmd: String,

    #[serde(
        default = "Profile::default_cwd",
        deserialize_with = "Profile::deserialize_cwd"
    )]
    pub cwd: String,
}

/// Structure describes either a custom font or whether to use the default system font. This
/// structure can be converted into a `pango::FontDescription`
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Font {
    #[serde(default = "Font::default_allow_bold")]
    pub allow_bold: bool,

    #[serde(default = "Font::default_size")]
    pub size: i32,

    #[serde(default = "Font::default_family")]
    pub family: String,

    #[serde(default = "Font::default_weight")]
    pub weight: FontWeight,
}

impl Profile {
    pub fn font(&self) -> (FontDescription, bool) {
        let mut font = FontDescription::new();
        font.set_family(&self.font.family);
        font.set_size(self.font.size * 1024);
        font.set_weight(self.font.weight.into());

        (font, self.font.allow_bold)
    }

    /// Break the command string into a series of PathBufs to pass them to vte-rs
    pub fn cmd(&self) -> Vec<PathBuf> {
        self.cmd.split(" ").map(PathBuf::from).collect::<Vec<_>>()
    }

    /// When deserializing the string for the command, parse any environment variables
    /// and check the first word in the string to see if it a) is a valid commmand path,
    /// or b) if that item exists in the path somewhere.
    fn deserialize_cmd<'de, D>(deserializer: D) -> result::Result<String, D::Error>
    where
        D: Deserializer<'de>,
    {
        let cmd = String::deserialize(deserializer)?;
        util::parse_cmd(cmd).ok_or(de::Error::custom("Bad command"))
    }

    /// When deserializing the string for the working directory, parse any environment
    /// variables and verify that it is a valid path.
    fn deserialize_cwd<'de, D>(deserializer: D) -> result::Result<String, D::Error>
    where
        D: Deserializer<'de>,
    {
        let cwd = String::deserialize(deserializer)?;
        util::parse_path(cwd)
            .and_then(|pathbuf| Some(pathbuf.to_string_lossy().deref().to_owned()))
            .ok_or(de::Error::custom("CWD not found!"))
    }

    #[doc(hidden)]
    fn default_scrollback() -> i64 {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.scrollback.to_owned();
        }
        1_000
    }

    #[doc(hidden)]
    fn default_cmd() -> String {
        env::var("SHELL").unwrap()
    }

    #[doc(hidden)]
    fn default_cwd() -> String {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.cwd.to_owned();
        }
        env::var("HOME").unwrap()
    }

    #[doc(hidden)]
    fn default_colorscheme() -> String {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.colorscheme.to_owned();
        }
        String::from("Base16 Dark Default")
    }

    #[doc(hidden)]
    fn default_cursor() -> CursorShape {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.cursor_style;
        }
        CursorShape::default()
    }

    #[doc(hidden)]
    fn default_blink() -> CursorBlinkMode {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.cursor_blink_mode;
        }
        CursorBlinkMode::default()
    }

    #[doc(hidden)]
    fn default_erase() -> EraseBinding {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.erase_binding;
        }
        EraseBinding::default()
    }

    #[doc(hidden)]
    fn default_bell() -> bool {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.audible_bell;
        }
        true
    }

    #[doc(hidden)]
    fn default_font() -> Font {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.font.to_owned();
        }
        Default::default()
    }

    #[doc(hidden)]
    fn default_dimensions() -> (u32, u32) {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.dimensions;
        }
        (80, 25)
    }

    #[doc(hidden)]
    fn default_cell_spacing() -> (f32, f32) {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.cell_spacing;
        }
        (1.0, 1.0)
    }
}

impl Font {
    #[doc(hidden)]
    fn default_size() -> i32 {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.font.size;
        }
        10
    }

    #[doc(hidden)]
    fn default_family() -> String {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.font.family.to_owned();
        }
        "Monospace".to_string()
    }

    #[doc(hidden)]
    fn default_allow_bold() -> bool {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.font.allow_bold;
        }
        true
    }

    #[doc(hidden)]
    fn default_weight() -> FontWeight {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.font.weight;
        }
        Default::default()
    }
}

impl Default for Profile {
    fn default() -> Self {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            Self {
                scrollback: default.scrollback,
                dimensions: default.dimensions,
                cell_spacing: default.cell_spacing,
                cursor_style: default.cursor_style,
                cursor_blink_mode: default.cursor_blink_mode,
                erase_binding: default.erase_binding,
                audible_bell: default.audible_bell,
                colorscheme: Profile::default_colorscheme(),
                font: default.font.to_owned(),
                cmd: Profile::default_cmd(),
                cwd: default.cwd.to_owned(),
            }
        } else {
            Self {
                scrollback: Profile::default_scrollback(),
                dimensions: Profile::default_dimensions(),
                cell_spacing: Profile::default_cell_spacing(),
                cursor_style: Default::default(),
                cursor_blink_mode: Default::default(),
                erase_binding: Default::default(),
                audible_bell: Default::default(),
                colorscheme: Profile::default_colorscheme(),
                font: Default::default(),
                cmd: Profile::default_cmd(),
                cwd: Profile::default_cwd(),
            }
        }
    }
}

impl Default for Font {
    fn default() -> Self {
        Self {
            allow_bold: Font::default_allow_bold(),
            size: Font::default_size(),
            family: Font::default_family(),
            weight: Font::default_weight(),
        }
    }
}
